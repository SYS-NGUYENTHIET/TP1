#include <stdio.h>
#define N 100000

int main()
{
    int i, j;
    for (i = 3; i <= N; i++)
    {
        j = 2;
        while ((j * j <= i) && (i % j != 0))
            j++;
        if (i % j != 0)
            printf("%d ", i);
    }
    printf("\n");
    return 0;
}